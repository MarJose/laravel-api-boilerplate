<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // == Front Desk ========
        Role::create(['name' => 'front desk'])
            ->givePermissionTo([
                /*system user*/
                'view system user',
                'view system user account profile',
                'add new system user',
                'delete system user',
                'block system user',
                'assign role and permission to user',
                'update role and permission to user',
                'update user',
                /*Roles and Permission*/
                'view role and permission',
                'add new role',
                'add permission to the role',
                'update role and permission',
                'delete role and permission',
                /* Import & Export Files*/
                'import files',
                'export files',


            ]);
    }
}
