## Login
---
System User login endpoint 

### System User Endpoint

> {warning} Please note that the URI for this endpoint only should include api/{$version} before oauth/token, which means that the url will be:
>`example.com/api/oauth/login`
