# Status Code

---

##### The following are the Status Codes for the API responses:

| # | Status Code | Status Name |
| : |   :       |  :          |
| 1 | 200         | Success    |
| 2 | 400         | Bad Request    |
| 3 | 401         | Authorization failed    |
| 4 | 402         | Validation Error    |
| 5 | 403         | Access denied    |
| 6 | 404         | Entity not found    |
| 7 | 419         | Token Expired    |
| 8 | 422         | Invalid Parameter    |
| 9 | 500         | Server error    |


