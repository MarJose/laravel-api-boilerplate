<?php
return [
  /*
   * This is the Language file for the ApiCode Response message
   * */
    /*
     |--------------------------------------------------------------------------
     | API Code Language Lines
     |--------------------------------------------------------------------------
     |
     | The following language lines are used by the ResponseBuilder to build
     | the message. You are free to change them to anything
     | you want to customize your views to better match your application.
     |
     */
  'access_denied' => 'You don\'t have enough privilege access profile to access, please ask help to your system administrator.',
  'bad_request' => 'Parameter is missing or not valid',
  'entity_not_found' => 'Unable to find on what you are looking for.',
  'authorization_failed'  => 'Unable to authorize your request or you don\'t have enough permission. ',
  'success'  => 'Job successfully finished.',
  'token_expired' => 'Token was already expired, please refresh or generate a new token.',
  'validation_error'  => 'Data is not valid for this request or required parameter is missing',
  'invalid_parameter'  => 'Parameter is incorrect.',
  'something_went_wrong' => 'Something went wrong while doing the job. Please manually check the system log for further investigation of the error',
  'server_error' => 'Oops!, There is an error the server, please try again later.'


];
