<?php


namespace App;

class ApiCode
{

    /*
    *
    * https://restfulapi.net/http-status-codes/
    */
    public const SOMETHING_WENT_WRONG = 250;
    public const SUCCESS = 200;
    public const BAD_REQUEST = 400;
    public const AUTHORIZATION_FAILED = 401;
    public const VALIDATION_ERROR = 402;
    public const ACCESS_DENIED = 403;
    public const ENTITY_NOT_FOUND = 404;
    public const TOKEN_EXPIRED = 419;
    public const INVALID_PARAMETER = 422;
    public const SERVER_ERROR = 500;
    public const TOKEN_NOT_PRESENT = 404;
    public const CREATED = 201;
    public const NO_CONTENT = 204;
    public const METHOD_NOT_ALLOWED = 405;
    public const NOT_ACCEPTABLE = 406;
    public const UNSUPPORTED_MEDIA_TYPE = 415;
}
