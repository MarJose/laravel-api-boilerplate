<?php

namespace App\Http\Controllers\API\Auth;

use App\ApiCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.jwt', ['except' => ['login']]);
    }
    //
    public function login(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return ResponseBuilder::asError(ApiCode::VALIDATION_ERROR)
                ->withMessage('Parameter is missing')
                ->withData($validator->errors()->toArray())
                ->build();
        }

        // get email and password from request
        $credentials = request(['email', 'password']);

        // try to auth and get the token using api authentication
        if (!$token = auth()->attempt($credentials)) {
            // if the credentials are wrong we send an unauthorized error in json format
            return ResponseBuilder::asError(ApiCode::AUTHORIZATION_FAILED)
                ->withMessage('Invalid Username / Password')
                ->build();
        }

        $data = [
            'token' => $token,
            'type' => 'bearer', // you can ommit this
            'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration
        ];
        return ResponseBuilder::asSuccess(ApiCode::SUCCESS)
            ->withData($data)
            ->withMessage('Please don\'t share this token to other people.')
            ->build();
    }
}
