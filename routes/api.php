<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->prefix('v1')->name('api.')->middleware(['api'])->group(static function() {

    Route::post('/auth/login', 'Auth\LoginController@login');

});

Route::namespace('API')->prefix('v1')->name('api.')->middleware(['auth.jwt, api'])->group(static function() {



});

